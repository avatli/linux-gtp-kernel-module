```
# This project has been opened to solve the problem encountered while performing the setup described in the link below.

https://www.slideshare.net/kentaroebisawa/using-gtp-on-linux-with-libgtpnl

$ cat /etc/os-release
NAME="Ubuntu"
VERSION="20.04 LTS (Focal Fossa)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 20.04 LTS"
VERSION_ID="20.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=focal
UBUNTU_CODENAME=focal

$ uname -r
5.4.0-40-generic

$ dmesg -C

$ ip link add veth1 type veth peer name veth2
$ ip addr add 172.0.0.1/24 dev veth1
$ ip link set veth1 up
$ ip addr add 172.99.0.1/32 dev lo

$ ./gtp-link add gtp1
WARNING: attaching dummy socket descriptors. Keep this process running for testing purposes.

---------------------------------------------------------

$ ./gtp-tunnel add gtp1 v1 200 100 172.99.0.2 172.0.0.2
$ ./gtp-tunnel list
version 1 tei 200/100 ms_addr 172.99.0.2 sgsn_addr 172.0.0.2
$ ip route add 172.99.0.2/32 dev gtp1

$ ip netns add ns2
$ ip link set veth2 netns ns2
$ ip netns exec ns2 ip addr add 172.0.0.2/24 dev veth2
$ ip netns exec ns2 ip link set veth2 up
$ ip netns exec ns2 ip addr add 172.99.0.2/32 dev lo
$ ip netns exec ns2 ip link set lo up
$ ip netns exec ns2 ./gtp-link add gtp2
WARNING: attaching dummy socket descriptors. Keep this process running for testing purposes.

---------------------------------------------------------

$ ip netns exec ns2 ./gtp-tunnel add gtp2 v1 100 200 172.99.0.1 172.0.0.1
$ ip netns exec ns2 ip route add 172.99.0.1/32 dev gtp2

$ dmesg
[  505.199419] gtp: GTP module loaded (pdp ctx size 104 bytes)
[ 1556.184521] IPv6: ADDRCONF(NETDEV_CHANGE): veth2: link becomes ready
[ 1556.184615] IPv6: ADDRCONF(NETDEV_CHANGE): veth1: link becomes ready

$ ifconfig 
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.200  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::ec4:7aff:fefd:f2fc  prefixlen 64  scopeid 0x20<link>
        ether 0c:c4:7a:fd:f2:fc  txqueuelen 1000  (Ethernet)
        RX packets 693  bytes 74736 (74.7 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 544  bytes 195154 (195.1 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device memory 0xdf360000-df37ffff  

gtp1: flags=4305<UP,POINTOPOINT,RUNNING,NOARP,MULTICAST>  mtu 0
        unspec 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00  txqueuelen 1000  (UNSPEC)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 96  bytes 7392 (7.3 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 96  bytes 7392 (7.3 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

veth1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.0.0.1  netmask 255.255.255.0  broadcast 0.0.0.0
        inet6 fe80::b4f2:7eff:feda:1976  prefixlen 64  scopeid 0x20<link>
        ether b6:f2:7e:da:19:76  txqueuelen 1000  (Ethernet)
        RX packets 10  bytes 796 (796.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 11  bytes 866 (866.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


$ ip netns exec ns2 ifconfig
gtp2: flags=4305<UP,POINTOPOINT,RUNNING,NOARP,MULTICAST>  mtu 0
        unspec 00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00  txqueuelen 1000  (UNSPEC)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

veth2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.0.0.2  netmask 255.255.255.0  broadcast 0.0.0.0
        inet6 fe80::cc84:6fff:fe90:3ced  prefixlen 64  scopeid 0x20<link>
        ether ce:84:6f:90:3c:ed  txqueuelen 1000  (Ethernet)
        RX packets 11  bytes 866 (866.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 11  bytes 866 (866.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet 172.99.0.1/32 scope global lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eno1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 0c:c4:7a:fd:f2:fc brd ff:ff:ff:ff:ff:ff
    inet 192.168.1.200/24 brd 192.168.1.255 scope global eno1
       valid_lft forever preferred_lft forever
    inet6 fe80::ec4:7aff:fefd:f2fc/64 scope link 
       valid_lft forever preferred_lft forever
3: eno2: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0c:c4:7a:fd:f2:fd brd ff:ff:ff:ff:ff:ff
4: eno3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0c:c4:7a:fd:f2:fe brd ff:ff:ff:ff:ff:ff
5: eno4: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0c:c4:7a:fd:f2:ff brd ff:ff:ff:ff:ff:ff
6: eno5: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0c:c4:7a:fd:f3:00 brd ff:ff:ff:ff:ff:ff
7: eno6: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0c:c4:7a:fd:f3:01 brd ff:ff:ff:ff:ff:ff
8: eno7: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0c:c4:7a:fd:f3:02 brd ff:ff:ff:ff:ff:ff
9: eno8: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 0c:c4:7a:fd:f3:03 brd ff:ff:ff:ff:ff:ff
11: veth1@if10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether b6:f2:7e:da:19:76 brd ff:ff:ff:ff:ff:ff link-netns ns2
    inet 172.0.0.1/24 scope global veth1
       valid_lft forever preferred_lft forever
    inet6 fe80::b4f2:7eff:feda:1976/64 scope link 
       valid_lft forever preferred_lft forever
13: gtp1: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 0 qdisc noqueue state UNKNOWN group default qlen 1000
    link/none 

$ ip netns exec ns2 ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet 172.99.0.2/32 scope global lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: gtp2: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 0 qdisc noqueue state UNKNOWN group default qlen 1000
    link/none 
10: veth2@if11: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether ce:84:6f:90:3c:ed brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.0.0.2/24 scope global veth2
       valid_lft forever preferred_lft forever
    inet6 fe80::cc84:6fff:fe90:3ced/64 scope link 
       valid_lft forever preferred_lft forever

$ route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.1.1     0.0.0.0         UG    0      0        0 eno1
172.0.0.0       0.0.0.0         255.255.255.0   U     0      0        0 veth1
172.99.0.2      0.0.0.0         255.255.255.255 UH    0      0        0 gtp1
192.168.1.0     0.0.0.0         255.255.255.0   U     0      0        0 eno1

$ ip netns exec ns2 route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
172.0.0.0       0.0.0.0         255.255.255.0   U     0      0        0 veth2
172.99.0.1      0.0.0.0         255.255.255.255 UH    0      0        0 gtp2

$ ping 172.99.0.2
PING 172.99.0.2 (172.99.0.2) 56(84) bytes of data.
ping: sendmsg: Network is unreachable
ping: sendmsg: Network is unreachable
^C
--- 172.99.0.2 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1013ms

$ ip netns exec ns2 ping 172.99.0.1
PING 172.99.0.1 (172.99.0.1) 56(84) bytes of data.
ping: sendmsg: Network is unreachable
ping: sendmsg: Network is unreachable
^C
--- 172.99.0.1 ping statistics ---
2 packets transmitted, 0 received, 100% packet loss, time 1002ms


$ modinfo gtp
filename:       /lib/modules/5.4.0-40-generic/kernel/drivers/net/gtp.ko
alias:          net-pf-16-proto-16-family-gtp
alias:          rtnl-link-gtp
description:    Interface driver for GTP encapsulated traffic
author:         Harald Welte <hwelte@sysmocom.de>
license:        GPL
srcversion:     520892508E92DEAF6E9C17E
depends:        udp_tunnel
retpoline:      Y
intree:         Y
name:           gtp
vermagic:       5.4.0-40-generic SMP mod_unload

$ modinfo udp_tunnel
filename:       /lib/modules/5.4.0-40-generic/kernel/net/ipv4/udp_tunnel.ko
license:        GPL
srcversion:     0A315BA6124B0664F4D23FB
depends:        
retpoline:      Y
intree:         Y
name:           udp_tunnel
vermagic:       5.4.0-40-generic SMP mod_unload
```
